package ru.alia.beeline;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class BeelineTest {
    private static WebDriver driver;
    private static Wait<WebDriver> wait;

    @BeforeAll
    static void init() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("enable-automation");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-gpu");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 5).withMessage("Element was not found");
    }

    @AfterAll
    static void clear() {
        driver.close();
    }

    @Test
    void beelineTest() throws Exception {
        // Перейти в браузере на marketing.beeline.ru
        driver.get("https://marketing.beeline.ru/");

        // Проверить, что доступно поле ввода телефона.
        WebElement textElement = driver.findElement(By.xpath("//input[@class='containers_phoneInput_3RBb']"));
        assertTrue(textElement.isEnabled());

        // Ввести свой телефон.
        textElement.sendKeys("89167386081");

        // Нажать кнопку «Получить SMS-код».
        WebElement buttonElement = driver.findElement(By.xpath("//button[@type='submit']"));
        buttonElement.click();

        // Проверить, что доступно поле ввода пароля.
        for (int i = 1; i <= 4; i++) {
            wait.until(visibilityOfElementLocated(
                    By.xpath("//input[" + i + "][@class='containers_field_1K-K null']")
            ));
        }
        WebElement divElement = driver.findElement(By.xpath("//div[@class='containers_smsContainer_2_kZ']"));
        List<WebElement> codeElements = divElement.findElements(By.xpath("//input[@class='containers_field_1K-K null']"));
        codeElements.forEach(webElement ->
                assertTrue(webElement.isEnabled())
        );

        //Ввести любые символы в поле кода.
        codeElements.forEach(webElement ->
                webElement.sendKeys(String.valueOf((int) (Math.random() * 10D) + 1))
        );
    }
}
